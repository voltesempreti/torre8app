package com.torre8;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Browser extends WebViewClient {
        public boolean overrideURLLoading (WebView view, String url){
            view.loadUrl(url);
            return true;
        }
    }